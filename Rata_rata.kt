fun main() {
    val input = java.util.Scanner(System.`in`)
    print("Masukkan jumlah angka yang akan dihitung rata-ratanya: ")
    val hitung = input.nextInt()
    val numbers = DoubleArray(hitung)

    for (i in 0 until hitung) {
        print("Masukkan angka ke-${i + 1}: ")
        numbers[i] = input.nextDouble()
    }

    val waktuMulai = System.currentTimeMillis()

    var sum = 0.0
    for (number in numbers) {
        sum += number
    }


    val ratarata = sum / numbers.size
    println("Rata-rata: $ratarata")

    val waktuBerhenti = System.currentTimeMillis()
    val durasi = waktuBerhenti - waktuMulai
    println("Waktu yang diperlukan : $durasi milisecond")
}